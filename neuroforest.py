import pylab as plt
import numpy as np
import LFPy
import neuron
import os

def return_pyramidal_cell(pos):
    cell_params = {
        'morphology': '2010-09-02Adjusted.hoc',
        'v_init': -77,             # initial crossmembrane potential
        #'e_pas' : -90,              # reversal potential passive mechs
        'passive': False,           # switch on passive mechs
        'nsegs_method': 'lambda_f',# method for setting number of segments,
        'lambda_f': 100,           # segments are isopotential at this frequenc
        'timeres_NEURON': 2**-1,   # dt of LFP and NEURON simulation.
        'timeres_python': 2**-1,
        'tstartms': -1,          #start time, recorders start at t=0
        'tstopms': 1,
        'pt3d': False,
    }
    cell = LFPy.Cell(**cell_params)
    cell.set_rotation(x=np.pi/2, z=2*np.pi*np.random.random())
    cell.set_pos(xpos=pos[0], zpos=pos[1])
    
    return cell

def plot_cell_to_axis(cell, ax, cell_idx, num_cells):
    
    idx_clr = lambda cell_idx: plt.cm.rainbow(int(256. * cell_idx / (num_cells - 1.)))
    
    for comp in xrange(cell.totnsegs):
        if comp == 0:
            ax.scatter(cell.xmid[comp], cell.zmid[comp], s=20, marker='^',
                       c=idx_clr(cell_idx), edgecolor='none', zorder=cell_idx)
        else:
            ax.plot([cell.xstart[comp], cell.xend[comp]], 
                    [cell.zstart[comp], cell.zend[comp]], 
                    color=idx_clr(cell_idx), lw=cell.diam[comp]/5, zorder=cell_idx)

def make_fig():

    plt.close('all')
    fig = plt.figure(figsize=[15, 5])
    ax_pop = fig.add_axes([0, 0, 1, 1], frameon=False,  aspect='equal')
    ax_pop.axis('off')
    ax_pop.patch.set_alpha(0.0)
    simple_neuron_forest(ax_pop)
    fig_name = 'NRSN'
    fig_num = 19
    fig.savefig('%s_%d.jpeg' % (fig_name, fig_num), dpi=500, facecolor='k')


def get_fig_num_from_folder(fig_name):
    file_list = [f.split('.')[0] for f in os.listdir('.') if fig_name in f]
    max_fig_num = np.max([0] + [int(f.split('_')[-1]) for f in file_list if f.split('_')[-1].isdigit()])
    return max_fig_num + 1

def simple_neuron_forest(ax):

    num_pyramidal = 120
    # plt.seed(123)

    xpos = 5 * 700 * (np.random.random(size=num_pyramidal) - 0.5)#np.random.normal(0, 100, size=num_pyramidal)
    ypos = 1.5*400 * (np.random.random(size=num_pyramidal) - 0.5)#np.random.normal(0, 50, size=num_pyramidal)

    plot_numbers = np.arange(num_pyramidal)
    np.random.shuffle(plot_numbers)
    for cell_idx in plot_numbers:
        neuron.h('forall delete_section()')
        cell = return_pyramidal_cell([xpos[cell_idx], ypos[cell_idx]])
        plot_cell_to_axis(cell, ax, cell_idx, num_pyramidal)
        #plot_numb +=1
    
if __name__ == '__main__':
    make_fig()
